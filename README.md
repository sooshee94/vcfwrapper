Instructions on how to build the project and run the docker container on a debian machine.

Prerequisite: Docker must be installed.

``` console
foo@bar:~$ sudo apt-get update
foo@bar:~$ sudo apt-get install default-jre -y
foo@bar:~$ sudo apt-get install default-jdk -y
foo@bar:~$ git clone https://gitlab.com/sooshee94/vcfwrapper
foo@bar:~$ cd vcfwrapper
foo@bar:~$ sudo ./gradlew clean assemble
foo@bar:~$ sudo docker build . -t clinvar
foo@bar:~$ sudo docker run -p 8080:8080 -t clinvar
```


If you want to get the latest image from dockerhub, then you can just skip most of the steps above and just run the following command:

``` console
foo@bar:~$ sudo docker run -p 8080:8080 -t sushiihd/bioinfo:dbsnp
```