FROM openjdk:8-jdk-alpine
VOLUME /tmp
ADD ./build/libs/gs-spring-boot-docker-0.1.0.jar app.jar
RUN wget ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/vcf_GRCh38/clinvar_20180729.vcf.gz
RUN wget ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/vcf_GRCh38/clinvar_20180729.vcf.gz.tbi
RUN cp /*.vcf.gz /vcf-file.vcf.gz
RUN cp /*.vcf.gz.tbi /vcf-file.vcf.gz.tbi
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
