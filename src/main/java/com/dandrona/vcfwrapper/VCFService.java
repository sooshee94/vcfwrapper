package com.dandrona.vcfwrapper;

import org.springframework.stereotype.Service;

@Service("vcfService")
public class VCFService {
    public DataLine readFileStartingFromPos(final String chromosomeNumber,
											final int position,
											final String expectedId,
											final String expectedRSID) {
		return MainApplication.getInstance().readFileStartingFromPos(chromosomeNumber, position, expectedId, expectedRSID);
    }
}