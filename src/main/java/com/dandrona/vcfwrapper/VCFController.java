package com.dandrona.vcfwrapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VCFController {
    @Autowired
    VCFService vcfService;

    @RequestMapping("/getRS")
    public @ResponseBody
	DataLine readFileStartingFromPos(@RequestParam(value = "chromosomeNumber") final String chromosomeNumber,
                                     @RequestParam(value = "position") final int position,
                                     @RequestParam(value = "expectedId", required = false) final String expectedId,
                                     @RequestParam(value = "expectedRSID", required = false) final String expectedRSID) {

        return vcfService.readFileStartingFromPos(chromosomeNumber, position, expectedId, expectedRSID);
    }
}
