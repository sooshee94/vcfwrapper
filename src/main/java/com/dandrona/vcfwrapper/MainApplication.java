package com.dandrona.vcfwrapper;

import htsjdk.samtools.util.BlockCompressedInputStream;
import htsjdk.tribble.index.Block;
import htsjdk.tribble.index.tabix.TabixIndex;
import org.springframework.util.StringUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

public class MainApplication {
	private static final String VCF_INDEX_FILE_PATH = "/vcf-file.vcf.gz.tbi";
	private static final String VCF_FILE_PATH = "/vcf-file.vcf.gz";
	private final static Logger LOGGER = Logger.getLogger(MainApplication.class.getName());
	private TabixIndex vcfIndex;
	private List<String> dbSNPHeaders;
	private static MainApplication instance = null;
	private MainApplication() {
		try {
			vcfIndex = new TabixIndex(new File(VCF_INDEX_FILE_PATH));
			String[] headers = {"CHROM", "POS", "ID", "REF", "ALT", "QUAL", "FILTER", "INFO"};
			dbSNPHeaders = Arrays.asList(headers);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static MainApplication getInstance() {
		if (instance == null)
			instance = new MainApplication();
		return instance;
	}

	public DataLine readFileStartingFromPos(String chromosomeNumber, int position, String expectedId, String expectedRSID) {
		List<Block> blocks = vcfIndex.getBlocks(chromosomeNumber, position, position);
		for (Block block : blocks) {
			long startPosition = block.getStartPosition();
			int linesParsed = 0;
			System.out.println(String.format("reading file %s starting from position %d and looking for %s or %s", VCF_FILE_PATH, startPosition, expectedId, expectedRSID));
			DataLine result = null;
			String line = "";
			try {
				BlockCompressedInputStream blockCompressedInputStream = new BlockCompressedInputStream(new File(VCF_FILE_PATH));
				blockCompressedInputStream.seek(startPosition);
				BufferedReader reader = new BufferedReader(new InputStreamReader(blockCompressedInputStream));
				while ((line = reader.readLine()) != null && linesParsed <= 2000) {
					if (expectedId != null) {
						if (!line.contains(expectedId))
							continue;
					}
					DataLine dataLine = getDataLine(line, dbSNPHeaders);
					if (!StringUtils.isEmpty(expectedId)) {
						if (expectedId.equals(dataLine.getID())) {
							result = dataLine;
							return result;
						}
					}
					else if (!StringUtils.isEmpty(expectedRSID)) {
						if (expectedRSID.equals(dataLine.getRSID())) {
							result = dataLine;
							return result;
						}
					}
					linesParsed++;
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}

		return null;
	}

	private DataLine getDataLine(String line, List<String> headers) {
		String[] split = line.split("\t");
		int length = split.length;
		DataLine dataLine = new DataLine();
		for (int i=0;i<headers.size();i++) {
			dataLine.setField(headers.get(i), split[i]);
		}
		return dataLine;
	}


}
